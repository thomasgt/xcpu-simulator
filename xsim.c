#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define X_INSTRUCTIONS_NOT_NEEDED

#include "xcpu.h"
#include "xis.h"

int main (int argc, char* argv[]) {
	// Verify that two arguments were provided
	if (argc != 3) {
		// Error, display message
		if (argc == 2) {
			printf("Error: This program requires 2 command line arguments, but %d was provided.\n", argc - 1);
		} else {
			printf("Error: This program requires 2 command line arguments, but %d were provided.\n", argc - 1);
		}
		return(-1);
	}
	
	// Load the file buffer
	FILE* inFile = fopen(argv[2], "r");
	// Check the file 
	if (inFile == NULL) {
		printf("The file, %s, could not be opened.\n", argv[2]);
		return(-1);
	}

	// Setup the automatic variables
	unsigned char memory[XIS_MEM_SIZE];	// simulated memory space
	memset(memory, 0, XIS_MEM_SIZE);
	
	// Load the file and track its size
	unsigned char* pch = memory;
	while (!feof(inFile)) {
		*(pch++) = fgetc(inFile);
	}
	// Close the file upon completion
	fclose(inFile);
	
	// Initialize the CPU registers and link memory 
	xcpu cpu = {
		.memory = memory
	};
	
	// Setup and run the execution cycle
	int maxCycles = atoi(argv[1]);		// maximum number of cycles to execute
	int cycleCount = 0;					// how many cycles have been executed
	while (cycleCount < maxCycles || !maxCycles) {
		if (xcpu_execute(&cpu) == 0) {
			cycleCount++;
		} else {
			printf("CPU has halted.\n");
			break;
		}	
	}
	return(0);
}
