# CSCI3120 - Operating Systems #
## Assignment 1 - May 24, 2015 ##
 Thomas Gwynne-Timothy - B00326579

The main directory contains the source code for the xcpu-simulator project. It also contains a directory named 'test-files' that houses 10 test cases, including their correspondong .xas files, their assembled .x files, the expected output in a .out file, the "gold" output in a .out.gold file, and finally a description in a .txt file.

To test the xsim program against the provided xsim_gold program, simply run the python script test.py. This script assembles each of the .xas test files, runs the files through both simulators, prints the output from each to a file, and compares the two files with diff.
