test.08.txt

This routine tests the stack, as well as the "not" instruction. Several 
comparisons are made using the "equ" instruction.

The expected results can be found in test.08.out.gold. The results from 
my simulator are found in test.08.out. The two files match.

