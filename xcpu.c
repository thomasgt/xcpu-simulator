/* File: xcpu.c
 * Date: May 11, 2015
 * Author: Thomas Gwynne-Timothy
 * Purpose: Follows the Von Neumann process for a single CPU execution. Takes
 * a reference to the X-CPU state via a pointer to a xcpu structure. Returns
 * zero if the execution was completed succesfully, or negative one otherwise. 
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#define MS_BYTE(x)    (((x) >> 8 ) & 0xff)	// Get MS-byte from WORD
#define LS_BYTE(x)    ((x) & 0xff)			// Get LS-byte from WORD

#include "xis.h"
#include "xcpu.h"

/* Declare functions for each instruction. While this takes up a lot of 
 * space (in the file), it allows us to make an array of function pointers. 
 * During the execution phase, the program can simply refer to the 
 * instruction as an element of the array. To do this, we has to remove 
 * the exclusion #define X_INSTRUCTIONS_NOT_NEEDED from the top of the 
 * file. This gave us an array containing the instruction codes that we 
 * can link to the array of function pointers.
 */

/////////////////////// ZERO-OPERAND INSTRUCTIONS //////////////////////
void x_ret( xcpu *, unsigned char );
void x_cld( xcpu *, unsigned char );
void x_std( xcpu *, unsigned char );
/////////////////////// ONE-OPERAND INSTRUCTIONS ///////////////////////
void x_neg( xcpu *, unsigned char );
void x_not( xcpu *, unsigned char );
void x_inc( xcpu *, unsigned char );
void x_dec( xcpu *, unsigned char );
void x_push( xcpu *, unsigned char );
void x_pop( xcpu *, unsigned char );
void x_jmpr( xcpu *, unsigned char );
void x_callr( xcpu *, unsigned char );
void x_out( xcpu *, unsigned char );
void x_br( xcpu *, unsigned char );
void x_jr( xcpu *, unsigned char );
/////////////////////// TWO-OPERAND INSTRUCTIONS ///////////////////////
void x_add( xcpu *, unsigned char );
void x_sub( xcpu *, unsigned char );
void x_mul( xcpu *, unsigned char );
void x_div( xcpu *, unsigned char );
void x_and( xcpu *, unsigned char );
void x_or( xcpu *, unsigned char );
void x_xor( xcpu *, unsigned char );
void x_shr( xcpu *, unsigned char );
void x_shl( xcpu *, unsigned char );
void x_test( xcpu *, unsigned char );
void x_cmp( xcpu *, unsigned char );
void x_equ( xcpu *, unsigned char );
void x_mov( xcpu *, unsigned char );
void x_load( xcpu *, unsigned char );
void x_stor( xcpu *, unsigned char );
void x_loadb( xcpu *, unsigned char );
void x_storb( xcpu *, unsigned char );
/////////////////////// EXT-OPERAND INSTRUCTIONS ///////////////////////
void x_jmp( xcpu *, unsigned char );
void x_call( xcpu *, unsigned char );
void x_loadi( xcpu *, unsigned char );
////////////////////// INSTRUCTION JUMP TABLE //////////////////////////
void (*instTable[]) (xcpu *cpu, unsigned char operand) = {
	x_ret,
	x_cld,
	x_std,

	x_neg,
	x_not,
	x_push,
	x_pop,
	x_jmpr,
	x_callr,
	x_out,
	x_inc,
	x_dec,
	x_br,
	x_jr,

	x_add,
	x_sub,
	x_mul,
	x_div,
	x_and,
	x_or,
	x_xor,
	x_shr,
	x_shl,
	x_test,
	x_cmp,
	x_equ,
	x_mov,
	x_load,
	x_stor,
	x_loadb,
	x_storb,

	x_jmp,
	x_call,
	x_loadi,
	
	NULL
};

void xcpu_print( xcpu *c ) {
	int i;

	fprintf( stdout, "PC: %4.4x, State: %4.4x: Registers:\n", c->pc, c->state );
	for( i = 0; i < X_MAX_REGS; i++ ) {
		fprintf( stdout, " %4.4x", c->regs[i] );
	}
	fprintf( stdout, "\n" );
}

extern int xcpu_execute( xcpu *c ) {
	// (1) Fetch the current instruction + operand
	unsigned char inst = c->memory[c->pc];
	unsigned char operand = c->memory[c->pc + 1];
	
	// (2) Decode the instruction
	unsigned short instId = 0;
	while (instId < I_NUM) {
		if (inst == x_instructions[instId].code) break;
		if (x_instructions[instId].code == 0) break;
		instId++;
	}
	
	// (3) Increment the program counter
	c->pc += 2;
	
	// (4) Execute the instruction
	if (instTable[instId] != NULL) {
		(*instTable[instId]) (c, operand);
	} else {
		return(-1);
	}
	
	// (5) Print registers to screen (if DEBUG_ON)
	if ((c->state & X_STATE_DEBUG_ON) == X_STATE_DEBUG_ON) xcpu_print(c);
	
	return(0);
}


/* Not needed for assignment 1 */
int xcpu_exception( xcpu *c, unsigned int ex ) {
	return 0;
}


/* Each of the following functions performs the pseudo-code provided
 * in the assignment. Please refer to the assignment handout. 
 */
/////////////////////// ZERO-OPERAND INSTRUCTIONS //////////////////////
void x_ret( xcpu *cpu, unsigned char operand ) {
	cpu->pc = ((unsigned short)cpu->memory[cpu->regs[X_STACK_REG]]) << 8 | cpu->memory[cpu->regs[X_STACK_REG] + 1];
	cpu->regs[X_STACK_REG] += 2;
}

void x_cld( xcpu *cpu, unsigned char operand ) {
	cpu->state &= ~X_STATE_DEBUG_ON;
}

void x_std( xcpu *cpu, unsigned char operand ) {
	cpu->state |= X_STATE_DEBUG_ON;	
}

/////////////////////// ONE-OPERAND INSTRUCTIONS ///////////////////////
void x_neg( xcpu *cpu, unsigned char operand ) {
	unsigned short D = (unsigned short)XIS_REG1(operand);
	cpu->regs[D] = -(cpu->regs[D]);
}

void x_not( xcpu *cpu, unsigned char operand ) {
	unsigned short D = (unsigned short)XIS_REG1(operand);
	cpu->regs[D] = !(cpu->regs[D]);
}

void x_inc( xcpu *cpu, unsigned char operand ) {
	unsigned short D = (unsigned short)XIS_REG1(operand);
	cpu->regs[D]++;
}

void x_dec( xcpu *cpu, unsigned char operand ) {
	unsigned short D = (unsigned short)XIS_REG1(operand);
	cpu->regs[D]--;
}

void x_push( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	cpu->regs[X_STACK_REG] -= 2;
	cpu->memory[cpu->regs[X_STACK_REG]] = (unsigned char)MS_BYTE(cpu->regs[S]);
	cpu->memory[cpu->regs[X_STACK_REG] + 1] = (unsigned char)LS_BYTE(cpu->regs[S]);
}

void x_pop( xcpu *cpu, unsigned char operand ) {
	unsigned short D = (unsigned short)XIS_REG1(operand);
	cpu->regs[D] = ((unsigned short)cpu->memory[cpu->regs[X_STACK_REG]]) << 8 | cpu->memory[cpu->regs[X_STACK_REG] + 1];
	cpu->regs[X_STACK_REG] += 2;
}

void x_jmpr( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	cpu->pc = cpu->regs[S];
}

void x_callr( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	cpu->regs[X_STACK_REG] -= 2;
	cpu->memory[cpu->regs[X_STACK_REG]] = (unsigned char)MS_BYTE(cpu->pc);
	cpu->memory[cpu->regs[X_STACK_REG] + 1] = (unsigned char)LS_BYTE(cpu->pc);
	cpu->pc = cpu->regs[S];		
}

void x_out( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	fprintf(stdout, "%c", (unsigned char)LS_BYTE(cpu->regs[S]));
}

void x_br( xcpu *cpu, unsigned char operand ) {
	if (cpu->state & X_STATE_COND_FLAG) {
		cpu->pc = (unsigned short)((short)cpu->pc + (char)operand) - 2;
		/* Subract 2 bytes from PC to compensate for early increment. */
	}
}

void x_jr( xcpu *cpu, unsigned char operand ) {
	cpu->pc = (unsigned short)((short)cpu->pc + (char)operand) - 2;
	/* Subract 2 bytes from PC to compensate for early increment. */
}

/////////////////////// TWO-OPERAND INSTRUCTIONS ///////////////////////
void x_add( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	unsigned short D = (unsigned short)XIS_REG2(operand);
	cpu->regs[D] += cpu->regs[S]; 
}

void x_sub( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	unsigned short D = (unsigned short)XIS_REG2(operand);
	cpu->regs[D] -= cpu->regs[S];	
}

void x_mul( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	unsigned short D = (unsigned short)XIS_REG2(operand);
	cpu->regs[D] *= cpu->regs[S];
}

void x_div( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	unsigned short D = (unsigned short)XIS_REG2(operand);
	cpu->regs[D] /= cpu->regs[S];
}

void x_and( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	unsigned short D = (unsigned short)XIS_REG2(operand);
	cpu->regs[D] &= cpu->regs[S];
}

void x_or( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	unsigned short D = (unsigned short)XIS_REG2(operand);
	cpu->regs[D] |= cpu->regs[S];
}

void x_xor( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	unsigned short D = (unsigned short)XIS_REG2(operand);
	cpu->regs[D] ^= cpu->regs[S];
}

void x_shr( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	unsigned short D = (unsigned short)XIS_REG2(operand);
	cpu->regs[D] >>= cpu->regs[S];
}

void x_shl( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	unsigned short D = (unsigned short)XIS_REG2(operand);
	cpu->regs[D] <<= cpu->regs[S];
}

void x_test( xcpu *cpu, unsigned char operand ) {
	unsigned short S1 = (unsigned short)XIS_REG1(operand);
	unsigned short S2 = (unsigned short)XIS_REG2(operand);
	if ((cpu->regs[S1] & cpu->regs[S2]) != 0) {
		cpu->state |= X_STATE_COND_FLAG;
	} else {
		cpu->state &= ~X_STATE_COND_FLAG;
	}
}

void x_cmp( xcpu *cpu, unsigned char operand ) {
	unsigned short S1 = (unsigned short)XIS_REG1(operand);
	unsigned short S2 = (unsigned short)XIS_REG2(operand);
	if (cpu->regs[S1] < cpu->regs[S2]) {
		cpu->state |= X_STATE_COND_FLAG;
	} else {
		cpu->state &= ~X_STATE_COND_FLAG;
	}
}

void x_equ( xcpu *cpu, unsigned char operand ) {
	unsigned short S1 = (unsigned short)XIS_REG1(operand);
	unsigned short S2 = (unsigned short)XIS_REG2(operand);
	if (cpu->regs[S1] == cpu->regs[S2]) {
		cpu->state |= X_STATE_COND_FLAG;
	} else {
		cpu->state &= ~X_STATE_COND_FLAG;
	}
}

void x_mov( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	unsigned short D = (unsigned short)XIS_REG2(operand);
	cpu->regs[D] = cpu->regs[S];
}

void x_load( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	unsigned short D = (unsigned short)XIS_REG2(operand);
	cpu->regs[D] = ((unsigned short)cpu->memory[cpu->regs[S]]) << 8 | cpu->memory[cpu->regs[S] + 1];
}

void x_stor( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	unsigned short D = (unsigned short)XIS_REG2(operand);
	cpu->memory[cpu->regs[D]] = (unsigned char)MS_BYTE(cpu->regs[S]);
	cpu->memory[cpu->regs[D] + 1] = (unsigned char)LS_BYTE(cpu->regs[S]);
}

void x_loadb( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	unsigned short D = (unsigned short)XIS_REG2(operand);
	cpu->regs[D] = (unsigned short)cpu->memory[cpu->regs[S]];
}

void x_storb( xcpu *cpu, unsigned char operand ) {
	unsigned short S = (unsigned short)XIS_REG1(operand);
	unsigned short D = (unsigned short)XIS_REG2(operand);
	cpu->memory[cpu->regs[D]] = (unsigned char)LS_BYTE(cpu->regs[S]);
}
/////////////////////// EXT-OPERAND INSTRUCTIONS ///////////////////////
void x_jmp( xcpu *cpu, unsigned char operand ) {
	cpu->pc = ((unsigned short)cpu->memory[cpu->pc]) << 8 | cpu->memory[cpu->pc + 1];
}

void x_call( xcpu *cpu, unsigned char operand ) {
	cpu->regs[X_STACK_REG] -= 2;
	cpu->memory[cpu->regs[X_STACK_REG]] = (unsigned char)MS_BYTE(cpu->pc + 2);
	cpu->memory[cpu->regs[X_STACK_REG] + 1] = (unsigned char)LS_BYTE(cpu->pc + 2);
	cpu->pc = ((unsigned short)cpu->memory[cpu->pc]) << 8 | cpu->memory[cpu->pc + 1];
}

void x_loadi( xcpu *cpu, unsigned char operand ) {
	unsigned short D = (unsigned short)XIS_REG1(operand);
	cpu->regs[D] = ((unsigned short)cpu->memory[cpu->pc]) << 8 | cpu->memory[cpu->pc + 1];
	(cpu->pc) += 2;
	/* Add 2 bytes to PC to compensate for extended instruction size. */
}
