# Python: 2.7.6
# Author: Thomas Gwynne-Timothy (B00326579)
# Purpose: This script can be used to test the xsim program against 
# the provided xsim_gold program. Make sure that the specified test 
# programs exist and have been assembled/linked into '.x' files
from subprocess import call

exeTest = "./xsim";
exeGold = "./xsim_gold";
asm = "./xas";
directory = "test-files/";
maxRuns = "100000";
programs = range(10)	#range(10) will test files 00 -> 09

print "Running X-CPU simulator test script..."
for i in range(len(programs)):
	# Setup files to take program output
	fileNameTest = ("test.%02d" % programs[i])+ ".out";
	fileNameGold = fileNameTest + ".gold";
	fileTest = open(directory + fileNameTest, "w+");
	fileGold = open(directory + fileNameGold, "w+");
	
	# Get the program to test
	xasFile = "test.%02d.xas" % programs[i];
	xFile = "test.%02d.x" % programs[i];
	
	# Assemble the test file
	print "Assembling test file " + xFile + "..."
	call([asm, directory + xasFile, directory + xFile]);
	
	# Run the program on the simulators
	call([exeTest, maxRuns, directory + xFile], stdout=fileTest)
	call([exeGold, maxRuns, directory + xFile], stdout=fileGold)
	
	# Compare the results
	print "Comparing output for", xFile, "with diff..."
	call(["diff", directory + fileNameGold, directory + fileNameTest])
	print "Comparison complete"
	
	# Close the files
	fileTest.close();
	fileGold.close();

print "Testing complete"
